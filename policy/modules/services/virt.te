policy_module(virt, 1.4.0)

########################################
#
# Declarations
#

attribute virsh_transition_domain;
attribute virt_ptynode;

## <desc>
##	<p>
##	Allow confined virtual guests to use serial/parallel communication ports
##	</p>
## </desc>
gen_tunable(virt_use_comm, false)

## <desc>
##	<p>
##	Allow confined virtual guests to use executable memory and executable stack
##	</p>
## </desc>
gen_tunable(virt_use_execmem, false)

## <desc>
##	<p>
##	Allow confined virtual guests to read fuse files
##	</p>
## </desc>
gen_tunable(virt_use_fusefs, false)

## <desc>
##	<p>
##	Allow confined virtual guests to manage nfs files
##	</p>
## </desc>
gen_tunable(virt_use_nfs, false)

## <desc>
##	<p>
##	Allow confined virtual guests to manage cifs files
##	</p>
## </desc>
gen_tunable(virt_use_samba, false)

## <desc>
##	<p>
##	Allow confined virtual guests to manage device configuration, (pci)
##	</p>
## </desc>
gen_tunable(virt_use_sysfs, false)

## <desc>
##  <p>
##  Allow confined virtual guests to interact with the sanlock
##  </p>
## </desc>
gen_tunable(virt_use_sanlock, false)

## <desc>
##	<p>
##	Allow confined virtual guests to interact with the xserver
##	</p>
## </desc>
gen_tunable(virt_use_xserver, false)

## <desc>
##	<p>
##	Allow confined virtual guests to use usb devices
##	</p>
## </desc>
gen_tunable(virt_use_usb, true)

virt_domain_template(svirt)
role system_r types svirt_t;
typealias svirt_t alias qemu_t;

attribute virt_domain;
attribute virt_image_type;
attribute virt_tmpfs_type;

type qemu_exec_t;

type virt_cache_t alias svirt_cache_t;
files_type(virt_cache_t)

type virt_etc_t;
files_config_file(virt_etc_t)

type virt_etc_rw_t;
files_type(virt_etc_rw_t)

type virt_home_t;
userdom_user_home_content(virt_home_t)

# virt Image files
type virt_image_t; # customizable
virt_image(virt_image_t)
files_mountpoint(virt_image_t)

# virt Image files
type virt_content_t; # customizable
virt_image(virt_content_t)
userdom_user_home_content(virt_content_t)

type virt_tmp_t;
files_tmp_file(virt_tmp_t)

type virt_log_t;
logging_log_file(virt_log_t)
mls_trusted_object(virt_log_t)

type virt_var_run_t;
files_pid_file(virt_var_run_t)

type virt_var_lib_t;
files_mountpoint(virt_var_lib_t)

type virtd_t;
type virtd_exec_t;
init_daemon_domain(virtd_t, virtd_exec_t)
domain_obj_id_change_exemption(virtd_t)
domain_subj_id_change_exemption(virtd_t)

type virtd_initrc_exec_t;
init_script_file(virtd_initrc_exec_t)

type qemu_var_run_t;
typealias qemu_var_run_t alias svirt_var_run_t;
files_pid_file(qemu_var_run_t)
mls_trusted_object(qemu_var_run_t)

ifdef(`enable_mcs',`
	init_ranged_daemon_domain(virtd_t, virtd_exec_t, s0 - mcs_systemhigh)
')

ifdef(`enable_mls',`
	init_ranged_daemon_domain(virtd_t, virtd_exec_t, s0 - mls_systemhigh)
')

type virt_qmf_t;
type virt_qmf_exec_t;
init_daemon_domain(virt_qmf_t, virt_qmf_exec_t)

########################################
#
# Declarations
#
attribute svirt_lxc_domain;

type virtd_lxc_t;
type virtd_lxc_exec_t;
init_system_domain(virtd_lxc_t, virtd_lxc_exec_t)

type virtd_lxc_var_run_t;
files_pid_file(virtd_lxc_var_run_t)

# virt lxc container files
type svirt_lxc_file_t;
files_mountpoint(svirt_lxc_file_t)

########################################
#
# svirt local policy
#

allow svirt_t self:udp_socket create_socket_perms;

read_lnk_files_pattern(svirt_t, virt_image_t, virt_image_t)

allow svirt_t svirt_image_t:dir search_dir_perms;
manage_dirs_pattern(svirt_t, svirt_image_t, svirt_image_t)
manage_files_pattern(svirt_t, svirt_image_t, svirt_image_t)
manage_fifo_files_pattern(svirt_t, svirt_image_t, svirt_image_t)
fs_hugetlbfs_filetrans(svirt_t, svirt_image_t, file)

list_dirs_pattern(svirt_t, virt_content_t, virt_content_t)
read_files_pattern(svirt_t, virt_content_t, virt_content_t)
dontaudit svirt_t virt_content_t:file write_file_perms;
dontaudit svirt_t virt_content_t:dir write;

corenet_udp_sendrecv_generic_if(svirt_t)
corenet_udp_sendrecv_generic_node(svirt_t)
corenet_udp_sendrecv_all_ports(svirt_t)
corenet_udp_bind_generic_node(svirt_t)
corenet_udp_bind_all_ports(svirt_t)
corenet_tcp_bind_all_ports(svirt_t)
corenet_tcp_connect_all_ports(svirt_t)

dev_list_sysfs(svirt_t)

fs_getattr_xattr_fs(svirt_t)

userdom_search_user_home_content(svirt_t)
userdom_read_user_home_content_symlinks(svirt_t)
userdom_read_all_users_state(svirt_t)
append_files_pattern(svirt_t, virt_home_t, virt_home_t)
stream_connect_pattern(svirt_t, virt_home_t, virt_home_t, virtd_t)

tunable_policy(`virt_use_comm',`
	term_use_unallocated_ttys(svirt_t)
	dev_rw_printer(svirt_t)
')

tunable_policy(`virt_use_fusefs',`
	fs_read_fusefs_files(svirt_t)
	fs_read_fusefs_symlinks(svirt_t)
')

tunable_policy(`virt_use_nfs',`
	fs_manage_nfs_dirs(svirt_t)
	fs_manage_nfs_files(svirt_t)
	fs_manage_nfs_named_sockets(svirt_t)
	fs_read_nfs_symlinks(svirt_t)
')

tunable_policy(`virt_use_samba',`
	fs_manage_cifs_dirs(svirt_t)
	fs_manage_cifs_files(svirt_t)
	fs_manage_cifs_named_sockets(svirt_t)
	fs_read_cifs_symlinks(virtd_t)
')

tunable_policy(`virt_use_sysfs',`
	dev_rw_sysfs(svirt_t)
')

tunable_policy(`virt_use_usb',`
	dev_rw_usbfs(svirt_t)
	dev_read_sysfs(svirt_t)
	fs_manage_dos_dirs(svirt_t)
	fs_manage_dos_files(svirt_t)
')

optional_policy(`
    tunable_policy(`virt_use_sanlock',`
        sanlock_stream_connect(svirt_t)
    ')
')

optional_policy(`
	tunable_policy(`virt_use_xserver',`
		xserver_stream_connect(svirt_t)
	')
')

optional_policy(`
	xen_rw_image_files(svirt_t)
')

########################################
#
# virtd local policy
#

allow virtd_t self:capability { chown dac_override fowner ipc_lock kill mknod net_admin net_raw setpcap setuid setgid sys_admin sys_nice };
allow virtd_t self:process { getcap getsched setcap sigkill signal signull execmem setexec setfscreate setsockcreate setsched };
ifdef(`hide_broken_symptoms',`
	# caused by some bogus kernel code
	dontaudit virtd_t self:capability sys_module;
')

allow virtd_t self:fifo_file { manage_fifo_file_perms relabelfrom relabelto };
allow virtd_t self:unix_stream_socket { connectto create_stream_socket_perms };
allow virtd_t self:tcp_socket create_stream_socket_perms;
allow virtd_t self:tun_socket { create_socket_perms relabelfrom relabelto };
allow virtd_t self:rawip_socket create_socket_perms;
allow virtd_t self:packet_socket create_socket_perms;
allow virtd_t self:netlink_kobject_uevent_socket create_socket_perms;

manage_dirs_pattern(virtd_t, virt_cache_t, virt_cache_t)
manage_files_pattern(virtd_t, virt_cache_t, virt_cache_t)

manage_dirs_pattern(virtd_t, virt_content_t, virt_content_t)
manage_files_pattern(virtd_t, virt_content_t, virt_content_t)

allow virtd_t virt_domain:process { getattr getsched setsched transition signal signull sigkill };
allow virt_domain virtd_t:fd use;
dontaudit virt_domain virtd_t:unix_stream_socket { read write };

can_exec(virtd_t, qemu_exec_t)
can_exec(virt_domain, qemu_exec_t)

allow virtd_t qemu_var_run_t:file relabel_file_perms;
manage_dirs_pattern(virtd_t, qemu_var_run_t, qemu_var_run_t)
manage_files_pattern(virtd_t, qemu_var_run_t, qemu_var_run_t)
manage_sock_files_pattern(virtd_t, qemu_var_run_t, qemu_var_run_t)
stream_connect_pattern(virtd_t, qemu_var_run_t, qemu_var_run_t, virt_domain)
filetrans_pattern(virtd_t, virt_var_run_t, qemu_var_run_t, dir, "qemu")

read_files_pattern(virtd_t, virt_etc_t, virt_etc_t)
read_lnk_files_pattern(virtd_t, virt_etc_t, virt_etc_t)

manage_dirs_pattern(virtd_t, virt_etc_rw_t, virt_etc_rw_t)
manage_files_pattern(virtd_t, virt_etc_rw_t, virt_etc_rw_t)
manage_lnk_files_pattern(virtd_t, virt_etc_rw_t, virt_etc_rw_t)
filetrans_pattern(virtd_t, virt_etc_t, virt_etc_rw_t, dir)

manage_files_pattern(virtd_t, virt_image_type, virt_image_type)
manage_chr_files_pattern(virtd_t, virt_image_type, virt_image_type)
manage_blk_files_pattern(virtd_t, virt_image_type, virt_image_type)
manage_lnk_files_pattern(virtd_t, virt_image_type, virt_image_type)
allow virtd_t virt_image_type:file relabel_file_perms;
allow virtd_t virt_image_type:blk_file relabel_blk_file_perms;
allow virtd_t virt_image_type:chr_file relabel_chr_file_perms;
allow virtd_t virt_ptynode:chr_file rw_term_perms;

manage_dirs_pattern(virtd_t, virt_tmp_t, virt_tmp_t)
manage_files_pattern(virtd_t, virt_tmp_t, virt_tmp_t)
files_tmp_filetrans(virtd_t, virt_tmp_t, { file dir })
can_exec(virtd_t, virt_tmp_t)

manage_dirs_pattern(virtd_t, virt_log_t, virt_log_t)
manage_files_pattern(virtd_t, virt_log_t, virt_log_t)
logging_log_filetrans(virtd_t, virt_log_t, { file dir })

manage_dirs_pattern(virtd_t, virt_var_lib_t, virt_var_lib_t)
manage_files_pattern(virtd_t, virt_var_lib_t, virt_var_lib_t)
manage_sock_files_pattern(virtd_t, virt_var_lib_t, virt_var_lib_t)
files_var_lib_filetrans(virtd_t, virt_var_lib_t, { file dir })

manage_dirs_pattern(virtd_t, virt_var_run_t, virt_var_run_t)
manage_files_pattern(virtd_t, virt_var_run_t, virt_var_run_t)
manage_sock_files_pattern(virtd_t, virt_var_run_t, virt_var_run_t)
files_pid_filetrans(virtd_t, virt_var_run_t, { file dir })

manage_dirs_pattern(virtd_t, virtd_lxc_var_run_t, virtd_lxc_var_run_t)
manage_files_pattern(virtd_t, virtd_lxc_var_run_t, virtd_lxc_var_run_t)
filetrans_pattern(virtd_t, virt_var_run_t, virtd_lxc_var_run_t, dir, "lxc")
stream_connect_pattern(virtd_t, virtd_lxc_var_run_t, virtd_lxc_var_run_t, virtd_lxc_t)

kernel_read_system_state(virtd_t)
kernel_read_network_state(virtd_t)
kernel_rw_net_sysctls(virtd_t)
kernel_read_kernel_sysctls(virtd_t)
kernel_request_load_module(virtd_t)
kernel_search_debugfs(virtd_t)

corecmd_exec_bin(virtd_t)
corecmd_exec_shell(virtd_t)

corenet_all_recvfrom_unlabeled(virtd_t)
corenet_all_recvfrom_netlabel(virtd_t)
corenet_tcp_sendrecv_generic_if(virtd_t)
corenet_tcp_sendrecv_generic_node(virtd_t)
corenet_tcp_sendrecv_all_ports(virtd_t)
corenet_tcp_bind_generic_node(virtd_t)
corenet_tcp_bind_virt_port(virtd_t)
corenet_tcp_bind_vnc_port(virtd_t)
corenet_tcp_connect_vnc_port(virtd_t)
corenet_tcp_connect_soundd_port(virtd_t)
corenet_rw_tun_tap_dev(virtd_t)

dev_rw_sysfs(virtd_t)
dev_read_urand(virtd_t)
dev_read_rand(virtd_t)
dev_rw_kvm(virtd_t)
dev_getattr_all_chr_files(virtd_t)
dev_rw_mtrr(virtd_t)
dev_rw_vhost(virtd_t)
dev_setattr_generic_usb_dev(virtd_t)
dev_relabel_generic_usb_dev(virtd_t)

# Init script handling
domain_use_interactive_fds(virtd_t)
domain_read_all_domains_state(virtd_t)
domain_read_all_domains_state(virtd_t)

files_read_usr_files(virtd_t)
files_read_etc_files(virtd_t)
files_read_usr_files(virtd_t)
files_read_etc_runtime_files(virtd_t)
files_search_all(virtd_t)
files_read_kernel_modules(virtd_t)
files_read_usr_src_files(virtd_t)
files_relabelto_system_conf_files(virtd_t)
files_relabelfrom_system_conf_files(virtd_t)

# Manages /etc/sysconfig/system-config-firewall
files_manage_system_conf_files(virtd_t)
files_etc_filetrans_system_conf(virtd_t)

fs_list_auto_mountpoints(virtd_t)
fs_getattr_xattr_fs(virtd_t)
fs_rw_anon_inodefs_files(virtd_t)
fs_list_inotifyfs(virtd_t)
fs_manage_cgroup_dirs(virtd_t)
fs_rw_cgroup_files(virtd_t)
fs_manage_hugetlbfs_dirs(virtd_t)
fs_rw_hugetlbfs_files(virtd_t)

mls_fd_share_all_levels(virtd_t)
mls_file_read_to_clearance(virtd_t)
mls_file_write_to_clearance(virtd_t)
mls_process_read_to_clearance(virtd_t)
mls_process_write_to_clearance(virtd_t)
mls_net_write_within_range(virtd_t)
mls_socket_write_to_clearance(virtd_t)
mls_socket_read_to_clearance(virtd_t)
mls_rangetrans_source(virtd_t)

mcs_process_set_categories(virtd_t)

storage_manage_fixed_disk(virtd_t)
storage_relabel_fixed_disk(virtd_t)
storage_raw_write_removable_device(virtd_t)
storage_raw_read_removable_device(virtd_t)

term_getattr_pty_fs(virtd_t)
term_use_generic_ptys(virtd_t)
term_use_ptmx(virtd_t)

auth_use_nsswitch(virtd_t)

init_dbus_chat(virtd_t)

miscfiles_read_localization(virtd_t)
miscfiles_read_generic_certs(virtd_t)
miscfiles_read_hwdata(virtd_t)

modutils_read_module_deps(virtd_t)
modutils_read_module_config(virtd_t)
modutils_manage_module_config(virtd_t)

logging_send_syslog_msg(virtd_t)
logging_send_audit_msgs(virtd_t)

selinux_validate_context(virtd_t)

seutil_read_config(virtd_t)
seutil_read_default_contexts(virtd_t)
seutil_read_file_contexts(virtd_t)

sysnet_domtrans_ifconfig(virtd_t)
sysnet_read_config(virtd_t)

userdom_list_admin_dir(virtd_t)
userdom_getattr_all_users(virtd_t)
userdom_list_user_home_content(virtd_t)
userdom_read_all_users_state(virtd_t)
userdom_read_user_home_content_files(virtd_t)
userdom_relabel_user_home_files(virtd_t)
userdom_setattr_user_home_content_files(virtd_t)
manage_dirs_pattern(virtd_t, virt_home_t, virt_home_t)
manage_files_pattern(virtd_t, virt_home_t, virt_home_t)
manage_sock_files_pattern(virtd_t, virt_home_t, virt_home_t)
manage_lnk_files_pattern(virtd_t, virt_home_t, virt_home_t)
#userdom_user_home_dir_filetrans(virtd_t, virt_home_t, { dir file })
virt_filetrans_home_content(virtd_t)

tunable_policy(`virt_use_nfs',`
	fs_manage_nfs_dirs(virtd_t)
	fs_manage_nfs_files(virtd_t)
	fs_read_nfs_symlinks(virtd_t)
')

tunable_policy(`virt_use_samba',`
	fs_manage_nfs_files(virtd_t)
	fs_manage_cifs_files(virtd_t)
	fs_read_cifs_symlinks(virtd_t)
')

optional_policy(`
	brctl_domtrans(virtd_t)
')

optional_policy(`
	consoletype_exec(virtd_t)
')

optional_policy(`
	dbus_system_bus_client(virtd_t)

	optional_policy(`
		avahi_dbus_chat(virtd_t)
	')

	optional_policy(`
		consolekit_dbus_chat(virtd_t)
	')

	optional_policy(`
		hal_dbus_chat(virtd_t)
	')

	optional_policy(`
		networkmanager_dbus_chat(virtd_t)
	')
')

optional_policy(`
	dmidecode_domtrans(virtd_t)
')

optional_policy(`
	dnsmasq_domtrans(virtd_t)
	dnsmasq_signal(virtd_t)
	dnsmasq_kill(virtd_t)
	dnsmasq_read_pid_files(virtd_t)
	dnsmasq_signull(virtd_t)
	dnsmasq_create_pid_dirs(virtd_t)
	dnsmasq_filetrans_named_content_fromdir(virtd_t, virt_var_run_t);
')

optional_policy(`
	iptables_domtrans(virtd_t)
	iptables_initrc_domtrans(virtd_t)
	iptables_systemctl(virtd_t)

	# Manages /etc/sysconfig/system-config-firewall
	iptables_manage_config(virtd_t)
')

optional_policy(`
	kerberos_keytab_template(virtd, virtd_t)
')

optional_policy(`
	lvm_domtrans(virtd_t)
')

optional_policy(`
	policykit_dbus_chat(virtd_t)
	policykit_domtrans_auth(virtd_t)
	policykit_domtrans_resolve(virtd_t)
	policykit_read_lib(virtd_t)
')

optional_policy(`
	qemu_exec(virtd_t)
')

optional_policy(`
	sanlock_stream_connect(virtd_t)
')

optional_policy(`
	sasl_connect(virtd_t)
')

optional_policy(`
	kernel_read_xen_state(virtd_t)
	kernel_write_xen_state(virtd_t)

	xen_stream_connect(virtd_t)
	xen_stream_connect_xenstore(virtd_t)
	xen_read_image_files(virtd_t)
')

optional_policy(`
	udev_domtrans(virtd_t)
	udev_read_db(virtd_t)
')

optional_policy(`
	unconfined_domain(virtd_t)
')

########################################
#
# virtual domains common policy
#

allow virt_domain self:process { signal getsched signull };
allow virt_domain self:fifo_file rw_fifo_file_perms;
allow virt_domain self:shm create_shm_perms;
allow virt_domain self:unix_stream_socket create_stream_socket_perms;
allow virt_domain self:unix_dgram_socket { create_socket_perms sendto };
allow virt_domain self:tcp_socket create_stream_socket_perms;

manage_dirs_pattern(virt_domain, virt_cache_t, virt_cache_t)
manage_files_pattern(virt_domain, virt_cache_t, virt_cache_t)
files_var_filetrans(virt_domain, virt_cache_t, { file dir })

manage_dirs_pattern(virt_domain, qemu_var_run_t, qemu_var_run_t)
manage_files_pattern(virt_domain, qemu_var_run_t, qemu_var_run_t)
manage_sock_files_pattern(virt_domain, qemu_var_run_t, qemu_var_run_t)
manage_lnk_files_pattern(virt_domain, qemu_var_run_t, qemu_var_run_t)
files_pid_filetrans(virt_domain, qemu_var_run_t, { dir file })
stream_connect_pattern(virt_domain, qemu_var_run_t, qemu_var_run_t, virtd_t)

dontaudit virtd_t virt_domain:process  { siginh noatsecure rlimitinh };

dontaudit virt_domain virt_tmpfs_type:file { read write };

append_files_pattern(virt_domain, virt_log_t, virt_log_t)

append_files_pattern(virt_domain, virt_var_lib_t, virt_var_lib_t)

kernel_read_system_state(virt_domain)

fs_getattr_xattr_fs(virt_domain)

corecmd_exec_bin(virt_domain)
corecmd_exec_shell(virt_domain)

corenet_all_recvfrom_unlabeled(virt_domain)
corenet_all_recvfrom_netlabel(virt_domain)
corenet_tcp_sendrecv_generic_if(virt_domain)
corenet_tcp_sendrecv_generic_node(virt_domain)
corenet_tcp_sendrecv_all_ports(virt_domain)
corenet_tcp_bind_generic_node(virt_domain)
corenet_tcp_bind_vnc_port(virt_domain)
corenet_tcp_bind_virt_migration_port(virt_domain)
corenet_tcp_connect_virt_migration_port(virt_domain)
corenet_rw_inherited_tun_tap_dev(virt_domain)

dev_read_generic_symlinks(virt_domain)
dev_read_rand(virt_domain)
dev_read_sound(virt_domain)
dev_read_urand(virt_domain)
dev_write_sound(virt_domain)
dev_rw_ksm(virt_domain)
dev_rw_kvm(virt_domain)
dev_rw_qemu(virt_domain)
dev_rw_inherited_vhost(virt_domain)

domain_use_interactive_fds(virt_domain)

files_read_etc_files(virt_domain)
files_read_mnt_symlinks(virt_domain)
files_read_usr_files(virt_domain)
files_read_var_files(virt_domain)
files_search_all(virt_domain)

fs_getattr_tmpfs(virt_domain)
fs_rw_anon_inodefs_files(virt_domain)
fs_rw_tmpfs_files(virt_domain)
fs_getattr_hugetlbfs(virt_domain)
fs_rw_inherited_nfs_files(virt_domain)
fs_rw_inherited_cifs_files(virt_domain)
fs_rw_inherited_noxattr_fs_files(virt_domain)

# I think we need these for now.
miscfiles_read_public_files(virt_domain)
storage_raw_read_removable_device(virt_domain)

term_use_all_inherited_terms(virt_domain)
term_getattr_pty_fs(virt_domain)
term_use_generic_ptys(virt_domain)
term_use_ptmx(virt_domain)

logging_send_syslog_msg(virt_domain)

miscfiles_read_localization(virt_domain)

tunable_policy(`virt_use_execmem',`
	allow virtd_t virt_domain:process { execmem execstack };
')

optional_policy(`
	ptchown_domtrans(virt_domain)
')

optional_policy(`
	pulseaudio_dontaudit_exec(virt_domain)
')

optional_policy(`
	virt_read_config(virt_domain)
	virt_read_lib_files(virt_domain)
	virt_read_content(virt_domain)
	virt_stream_connect(virt_domain)
')

########################################
#
# xm local policy
#
type virsh_t;
type virsh_exec_t;
init_system_domain(virsh_t, virsh_exec_t)
typealias virsh_t alias xm_t;
typealias virsh_exec_t alias xm_exec_t;

allow virsh_t self:capability { setpcap dac_override ipc_lock sys_tty_config };
allow virsh_t self:process { getcap getsched setsched setcap signal };
allow virsh_t self:fifo_file rw_fifo_file_perms;
allow virsh_t self:unix_stream_socket { create_stream_socket_perms connectto };
allow virsh_t self:tcp_socket create_stream_socket_perms;

manage_files_pattern(virsh_t, virt_image_type, virt_image_type)
manage_blk_files_pattern(virsh_t, virt_image_type, virt_image_type)
manage_lnk_files_pattern(virsh_t, virt_image_type, virt_image_type)

dontaudit virsh_t virt_var_lib_t:file read_inherited_file_perms;

kernel_read_system_state(virsh_t)
kernel_read_network_state(virsh_t)
kernel_read_kernel_sysctls(virsh_t)
kernel_read_sysctl(virsh_t)
kernel_read_xen_state(virsh_t)
kernel_write_xen_state(virsh_t)

corecmd_exec_bin(virsh_t)
corecmd_exec_shell(virsh_t)

corenet_tcp_sendrecv_generic_if(virsh_t)
corenet_tcp_sendrecv_generic_node(virsh_t)
corenet_tcp_connect_soundd_port(virsh_t)

dev_read_rand(virsh_t)
dev_read_urand(virsh_t)
dev_read_sysfs(virsh_t)

files_read_etc_runtime_files(virsh_t)
files_read_usr_files(virsh_t)
files_list_mnt(virsh_t)
# Some common macros (you might be able to remove some)
files_read_etc_files(virsh_t)

fs_getattr_all_fs(virsh_t)
fs_manage_xenfs_dirs(virsh_t)
fs_manage_xenfs_files(virsh_t)
fs_search_auto_mountpoints(virsh_t)

storage_raw_read_fixed_disk(virsh_t)

term_use_all_inherited_terms(virsh_t)

init_stream_connect_script(virsh_t)
init_rw_script_stream_sockets(virsh_t)
init_use_fds(virsh_t)

miscfiles_read_localization(virsh_t)

sysnet_dns_name_resolve(virsh_t)

optional_policy(`
	xen_manage_image_dirs(virsh_t)
	xen_append_log(virsh_t)
	xen_domtrans(virsh_t)
	xen_read_pid_files_xenstored(virsh_t)
	xen_stream_connect(virsh_t)
	xen_stream_connect_xenstore(virsh_t)
')

optional_policy(`
	dbus_system_bus_client(virsh_t)

	optional_policy(`
		hal_dbus_chat(virsh_t)
	')
')

optional_policy(`
	vhostmd_rw_tmpfs_files(virsh_t)
	vhostmd_stream_connect(virsh_t)
	vhostmd_dontaudit_rw_stream_connect(virsh_t)
')

optional_policy(`
	virt_domtrans(virsh_t)
	virt_manage_images(virsh_t)
	virt_manage_config(virsh_t)
	virt_stream_connect(virsh_t)
')

optional_policy(`
	ssh_basic_client_template(virsh, virsh_t, system_r)

	kernel_read_xen_state(virsh_ssh_t)
	kernel_write_xen_state(virsh_ssh_t)

	dontaudit virsh_ssh_t virsh_transition_domain:fifo_file rw_inherited_fifo_file_perms;
	files_search_tmp(virsh_ssh_t)

	fs_manage_xenfs_dirs(virsh_ssh_t)
	fs_manage_xenfs_files(virsh_ssh_t)

	userdom_search_admin_dir(virsh_ssh_t)
')

########################################
#
# virt_lxc local policy
#
allow virtd_lxc_t self:capability { dac_override net_admin net_raw setpcap chown sys_admin };
allow virtd_lxc_t self:process { setsched getcap setcap signal_perms };
allow virtd_lxc_t self:fifo_file rw_fifo_file_perms;
allow virtd_lxc_t self:netlink_route_socket rw_netlink_socket_perms;
allow virtd_lxc_t self:unix_stream_socket create_stream_socket_perms;
allow virtd_lxc_t self:packet_socket create_socket_perms;

allow virtd_lxc_t virt_image_type:dir mounton;
manage_files_pattern(virtd_lxc_t, virt_image_t, virt_image_t)

domtrans_pattern(virtd_t, virtd_lxc_exec_t, virtd_lxc_t)
allow virtd_t virtd_lxc_t:process { signal signull sigkill };

allow virtd_lxc_t virt_var_run_t:dir search_dir_perms;
manage_dirs_pattern(virtd_lxc_t, virtd_lxc_var_run_t, virtd_lxc_var_run_t)
manage_files_pattern(virtd_lxc_t, virtd_lxc_var_run_t, virtd_lxc_var_run_t)
manage_sock_files_pattern(virtd_lxc_t, virtd_lxc_var_run_t, virtd_lxc_var_run_t)
files_pid_filetrans(virtd_lxc_t, virtd_lxc_var_run_t, { file dir })

manage_dirs_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_file_t)
manage_files_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_file_t)
manage_chr_files_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_file_t)
manage_lnk_files_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_file_t)
manage_sock_files_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_file_t)
manage_fifo_files_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_file_t)
allow virtd_lxc_t svirt_lxc_file_t:dir_file_class_set { relabelto relabelfrom };

storage_manage_fixed_disk(virtd_lxc_t)

kernel_read_network_state(virtd_lxc_t)
kernel_search_network_sysctl(virtd_lxc_t)
kernel_read_sysctl(virtd_lxc_t)
kernel_read_system_state(virtd_lxc_t)

corecmd_exec_bin(virtd_lxc_t)
corecmd_exec_shell(virtd_lxc_t)

dev_read_sysfs(virtd_lxc_t)
dev_relabel_all_dev_nodes(virtd_lxc_t)

domain_use_interactive_fds(virtd_lxc_t)

files_read_etc_files(virtd_lxc_t)
files_read_usr_files(virtd_lxc_t)
files_mounton_non_security(virtd_lxc_t)
files_mount_all_file_type_fs(virtd_lxc_t)
files_unmount_all_file_type_fs(virtd_lxc_t)
files_list_isid_type_dirs(virtd_lxc_t)

fs_manage_tmpfs_dirs(virtd_lxc_t)
fs_manage_tmpfs_chr_files(virtd_lxc_t)
fs_manage_tmpfs_symlinks(virtd_lxc_t)
fs_manage_cgroup_dirs(virtd_lxc_t)
fs_rw_cgroup_files(virtd_lxc_t)
fs_remount_all_fs(virtd_lxc_t)
fs_unmount_xattr_fs(virtd_lxc_t)

selinux_mount_fs(virtd_lxc_t)
selinux_unmount_fs(virtd_lxc_t)

term_use_generic_ptys(virtd_lxc_t)
term_use_ptmx(virtd_lxc_t)

auth_use_nsswitch(virtd_lxc_t)

logging_send_syslog_msg(virtd_lxc_t)

miscfiles_read_localization(virtd_lxc_t)

sysnet_domtrans_ifconfig(virtd_lxc_t)

#optional_policy(`
#	unconfined_shell_domtrans(virtd_lxc_t)
#	unconfined_signal(virtd_t)
#')

########################################
#
# virt_lxc_domain local policy
#
allow svirt_lxc_domain self:capability { kill setuid setgid dac_override };

allow virtd_t svirt_lxc_domain:process { signal_perms };
allow virtd_lxc_t svirt_lxc_domain:process { getattr getsched setsched transition signal signull sigkill };

allow svirt_lxc_domain virtd_lxc_t:fd use;
allow svirt_lxc_domain virtd_lxc_var_run_t:dir search_dir_perms;
dontaudit svirt_lxc_domain virtd_lxc_t:unix_stream_socket { read write };

allow svirt_lxc_domain self:process { getattr signal_perms getsched setsched setcap setpgid execstack execmem };
allow svirt_lxc_domain self:fifo_file manage_file_perms;
allow svirt_lxc_domain self:sem create_sem_perms;
allow svirt_lxc_domain self:shm create_shm_perms;
allow svirt_lxc_domain self:msgq create_msgq_perms;
allow svirt_lxc_domain self:unix_stream_socket create_stream_socket_perms;
allow svirt_lxc_domain self:unix_dgram_socket { sendto create_socket_perms };
dontaudit svirt_lxc_domain self:netlink_audit_socket { create_netlink_socket_perms nlmsg_relay };

manage_dirs_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
manage_files_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
manage_lnk_files_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
manage_sock_files_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
manage_fifo_files_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
rw_chr_files_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
rw_blk_files_pattern(svirt_lxc_domain, svirt_lxc_file_t, svirt_lxc_file_t)
can_exec(svirt_lxc_domain, svirt_lxc_file_t)

kernel_getattr_proc(svirt_lxc_domain)
kernel_read_kernel_sysctls(svirt_lxc_domain)
kernel_read_system_state(svirt_lxc_domain)
kernel_dontaudit_search_kernel_sysctl(svirt_lxc_domain)

corecmd_exec_all_executables(svirt_lxc_domain)

dev_read_urand(svirt_lxc_domain)
dev_dontaudit_read_rand(svirt_lxc_domain)
dev_read_sysfs(svirt_lxc_domain)

files_dontaudit_list_all_mountpoints(svirt_lxc_domain)
files_entrypoint_all_files(svirt_lxc_domain)
files_search_all(svirt_lxc_domain)
files_read_config_files(svirt_lxc_domain)
files_read_usr_files(svirt_lxc_domain)
files_read_usr_symlinks(svirt_lxc_domain)

fs_getattr_tmpfs(svirt_lxc_domain)
fs_getattr_xattr_fs(svirt_lxc_domain)
fs_list_inotifyfs(svirt_lxc_domain)
fs_dontaudit_getattr_xattr_fs(svirt_lxc_domain)

auth_dontaudit_read_passwd(svirt_lxc_domain)
auth_dontaudit_read_login_records(svirt_lxc_domain)
auth_dontaudit_write_login_records(svirt_lxc_domain)
auth_search_pam_console_data(svirt_lxc_domain)

init_read_utmp(svirt_lxc_domain)
init_dontaudit_write_utmp(svirt_lxc_domain)

libs_dontaudit_setattr_lib_files(svirt_lxc_domain)

miscfiles_read_localization(svirt_lxc_domain)
miscfiles_dontaudit_setattr_fonts_cache_dirs(svirt_lxc_domain)

mta_dontaudit_read_spool_symlinks(svirt_lxc_domain)

selinux_get_fs_mount(svirt_lxc_domain)
selinux_validate_context(svirt_lxc_domain)
selinux_compute_access_vector(svirt_lxc_domain)
selinux_compute_create_context(svirt_lxc_domain)
selinux_compute_relabel_context(svirt_lxc_domain)
selinux_compute_user_contexts(svirt_lxc_domain)
seutil_read_default_contexts(svirt_lxc_domain)

miscfiles_read_fonts(svirt_lxc_domain)

optional_policy(`
	apache_exec_modules(svirt_lxc_domain)
')

virt_lxc_domain_template(svirt_lxc_net)

allow svirt_lxc_net_t self:udp_socket create_socket_perms;
allow svirt_lxc_net_t self:tcp_socket create_stream_socket_perms;
allow svirt_lxc_net_t self:netlink_route_socket create_netlink_socket_perms;
allow svirt_lxc_net_t self:packet_socket create_socket_perms;
allow svirt_lxc_net_t self:udp_socket create_socket_perms;

corenet_tcp_bind_generic_node(svirt_lxc_net_t)
corenet_udp_bind_generic_node(svirt_lxc_net_t)

allow svirt_lxc_net_t self:capability { net_raw net_admin net_bind_service };
corenet_tcp_sendrecv_all_ports(svirt_lxc_net_t)
corenet_udp_sendrecv_all_ports(svirt_lxc_net_t)
corenet_udp_bind_all_ports(svirt_lxc_net_t)
corenet_tcp_bind_all_ports(svirt_lxc_net_t)
corenet_tcp_connect_all_ports(svirt_lxc_net_t)
kernel_read_network_state(svirt_lxc_net_t)

domain_entry_file(svirt_lxc_net_t, svirt_lxc_file_t)
domtrans_pattern(virtd_lxc_t, svirt_lxc_file_t, svirt_lxc_net_t)
corecmd_shell_domtrans(virtd_lxc_t, svirt_lxc_net_t)
fs_noxattr_type(svirt_lxc_file_t)
term_pty(svirt_lxc_file_t)

########################################
#
# virt_qmf local policy
#
allow virt_qmf_t self:process signal;
allow virt_qmf_t self:fifo_file rw_fifo_file_perms;
allow virt_qmf_t self:unix_stream_socket create_stream_socket_perms;
allow virt_qmf_t self:tcp_socket create_stream_socket_perms;

kernel_read_network_state(virt_qmf_t)

dev_list_sysfs(virt_qmf_t)
dev_read_sysfs(virt_qmf_t)

corenet_tcp_connect_matahari_port(virt_qmf_t)

domain_use_interactive_fds(virt_qmf_t)

files_read_etc_files(virt_qmf_t)

logging_send_syslog_msg(virt_qmf_t)

miscfiles_read_localization(virt_qmf_t)
