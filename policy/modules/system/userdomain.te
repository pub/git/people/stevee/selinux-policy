policy_module(userdomain, 4.5.2)

########################################
#
# Declarations
#

## <desc>
## <p>
## Allow users to connect to the local mysql server
## </p>
## </desc>
gen_tunable(allow_user_mysql_connect, false)

## <desc>
## <p>
## Allow users to connect to PostgreSQL
## </p>
## </desc>
gen_tunable(allow_user_postgresql_connect, false)

## <desc>
## <p>
## Allow regular users direct mouse access
## </p>
## </desc>
gen_tunable(user_direct_mouse, false)

## <desc>
## <p>
## Allow users to read system messages.
## </p>
## </desc>
gen_tunable(user_dmesg, false)

## <desc>
## <p>
## Allow user to r/w files on filesystems
## that do not have extended attributes (FAT, CDROM, FLOPPY)
## </p>
## </desc>
gen_tunable(user_rw_noexattrfile, false)

## <desc>
## <p>
## Allow user music sharing
## </p>
## </desc>
gen_tunable(user_share_music, false)

## <desc>
## <p>
## Allow user processes to change their priority 
## </p>
## </desc>
gen_tunable(user_setrlimit, false)

## <desc>
## <p>
## Allow w to display everyone
## </p>
## </desc>
gen_tunable(user_ttyfile_stat, false)

attribute admindomain;

# all user domains
attribute userdomain;

# unprivileged user domains
attribute unpriv_userdomain;

attribute untrusted_content_type;
attribute untrusted_content_tmp_type;

attribute userdom_home_reader_type;
attribute userdom_home_manager_type;

# unprivileged user domains
attribute user_home_type;
attribute user_tmp_type;
attribute user_tmpfs_type;

type admin_home_t;
files_type(admin_home_t)
files_associate_tmp(admin_home_t)
fs_associate_tmpfs(admin_home_t)
files_mountpoint(admin_home_t)
files_poly_member(admin_home_t)
files_poly_parent(admin_home_t)

type user_home_dir_t alias { staff_home_dir_t sysadm_home_dir_t secadm_home_dir_t auditadm_home_dir_t unconfined_home_dir_t };
fs_associate_tmpfs(user_home_dir_t)
files_type(user_home_dir_t)
files_mountpoint(user_home_dir_t)
files_associate_tmp(user_home_dir_t)
files_poly(user_home_dir_t)
files_poly_member(user_home_dir_t)
files_poly_parent(user_home_dir_t)
ubac_constrained(user_home_dir_t)

type user_home_t alias { staff_home_t sysadm_home_t secadm_home_t auditadm_home_t unconfined_home_t };
typealias user_home_t alias { staff_untrusted_content_t sysadm_untrusted_content_t secadm_untrusted_content_t auditadm_untrusted_content_t unconfined_untrusted_content_t };
typeattribute user_home_t user_home_type;
userdom_user_home_content(user_home_t)
fs_associate_tmpfs(user_home_t)
files_associate_tmp(user_home_t)
files_poly_member(user_home_t)
files_poly_parent(user_home_t)
files_mountpoint(user_home_t)
ubac_constrained(user_home_t)

type user_devpts_t alias { staff_devpts_t sysadm_devpts_t secadm_devpts_t auditadm_devpts_t unconfined_devpts_t };
dev_node(user_devpts_t)
files_type(user_devpts_t)
ubac_constrained(user_devpts_t)

type user_tmp_t, user_tmp_type;
typealias user_tmp_t alias { winbind_tmp_t sshd_tmp_t staff_tmp_t sysadm_tmp_t secadm_tmp_t auditadm_tmp_t unconfined_tmp_t };
typealias user_tmp_t alias { staff_untrusted_content_tmp_t sysadm_untrusted_content_tmp_t secadm_untrusted_content_tmp_t auditadm_untrusted_content_tmp_t unconfined_untrusted_content_tmp_t };
files_tmp_file(user_tmp_t)
userdom_user_home_content(user_tmp_t)
files_poly_parent(user_tmp_t)

type user_tmpfs_t, user_tmpfs_type;
typealias user_tmpfs_t alias { staff_tmpfs_t sysadm_tmpfs_t secadm_tmpfs_t auditadm_tmpfs_t unconfined_tmpfs_t };
files_tmpfs_file(user_tmpfs_t)
userdom_user_home_content(user_tmpfs_t)

type user_tty_device_t alias { staff_tty_device_t sysadm_tty_device_t secadm_tty_device_t auditadm_tty_device_t unconfined_tty_device_t };
dev_node(user_tty_device_t)
ubac_constrained(user_tty_device_t)

type audio_home_t;
userdom_user_home_content(audio_home_t)
ubac_constrained(audio_home_t)

type home_bin_t;
userdom_user_home_content(home_bin_t)
ubac_constrained(home_bin_t)

type home_cert_t;
miscfiles_cert_type(home_cert_t)
userdom_user_home_content(home_cert_t)
ubac_constrained(home_cert_t)

tunable_policy(`allow_console_login',`
	term_use_console(userdomain)
')

allow userdomain userdomain:process signull;

# Nautilus causes this avc
dontaudit unpriv_userdomain self:dir setattr;
allow unpriv_userdomain self:key manage_key_perms;

optional_policy(`
	alsa_read_rw_config(unpriv_userdomain)
	alsa_manage_home_files(unpriv_userdomain)
	alsa_relabel_home_files(unpriv_userdomain)
')

optional_policy(`
	ssh_filetrans_home_content(userdomain)
')

optional_policy(`
	xserver_filetrans_home_content(userdomain)
')


tunable_policy(`use_nfs_home_dirs',`
    fs_read_nfs_files(userdom_home_reader_type)
')

tunable_policy(`use_samba_home_dirs',`
    fs_read_cifs_files(userdom_home_reader_type)
')

tunable_policy(`use_fusefs_home_dirs',`
    fs_read_fusefs_files(userdom_home_reader_type)
')

tunable_policy(`use_nfs_home_dirs',`
    fs_list_auto_mountpoints(userdom_home_manager_type)
    fs_manage_nfs_dirs(userdom_home_manager_type)
    fs_manage_nfs_files(userdom_home_manager_type)
    fs_manage_nfs_symlinks(userdom_home_manager_type)
')

tunable_policy(`use_samba_home_dirs',`
    fs_manage_cifs_dirs(userdom_home_manager_type)
    fs_manage_cifs_files(userdom_home_manager_type)
    fs_manage_cifs_symlinks(userdom_home_manager_type)
')

tunable_policy(`use_fusefs_home_dirs',`
    fs_manage_fusefs_dirs(userdom_home_manager_type)
    fs_manage_fusefs_files(userdom_home_manager_type)
    fs_manage_fusefs_symlinks(userdom_home_manager_type)
')

